
lista="`g.list  vect |cat | tr ' ' '\n' | grep gpx`"

#for i in $lista; do g.remove vect=$i; done
for i in gpx/*
do
  f=$i
  bname=$(basename $f | rev | cut -f2- -d. | rev)
  gpsbabel -i gpx -f $f -x transform,rte=trk -o gpx -F gpx/$bname.routes
  v.in.gpsbabel -r input=gpx/$bname.routes output=gpx_$bname format=gpx --overwrite
done
#g.list vect

list="`g.list  vect |cat | tr ' ' '\n' | grep gpx_ | tr '\n' ','`"

v.patch input=$list output=allgpx --overwrite

v.clean input=allgpx output=allgpxclean type=point,line,boundary,centroid,area tool=break,snap --overwrite 



