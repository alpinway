#!/usr/bin/python

# Author: Stepan Kafka

import os,time,string,sys,Gnuplot,re
from pywps.Process.Process import WPSProcess

class Process(WPSProcess):
    def __init__(self):
        WPSProcess.__init__(self,
		identifier = "shortestpath",
        	title="Shortest path",
		abstract="Find the shortest path on the Trentino SAT network",
		version = "0.2",
        	storeSupported = True,
		statusSupported = True,
        	grassLocation="alpi")
        
	self.cost = self.addLiteralInput(identifier ="cost",
			title = "Start x coordinate",
			type = type(0.0),
			maxOccurs = 200)
	self.coord = self.addLiteralInput(identifier ="coord",
			title = "Points",
			type = type("*"),
			minOccurs =4,
			maxOccurs =200)

        self.los = self.addComplexOutput(identifier = "los",
                            title = "Resulting output map",
                            formats = [{"mimeType":"text/xml"}])
    
    def execute(self):
	
	self.cmd("g.mapset mapset=mapset")
        self.cmd("g.region -d")
	self.status.set(msg="Region setted", percentDone=20)
	lpoints=self.coord.value[0].split(",")

	#Points are stored in lpoints
	#we must find every shortest path between two consecutive point and
	#put all together
	self.cmd("v.in.ascii -e output=path")
	npoints=len(lpoints)/2
	#the number of loop cycle in which shortest path is called is npoints-1
	x1=lpoints[0]
	y1=lpoints[1]
	for i in range(2,2*npoints-1,2):
		x2=lpoints[i]
		y2=lpoints[i+1]
		#Find the shortest path between the two points
		#self.cmd("echo ""0 %s %s %s %s"" | v.net.path input=allgpx output=segment --o" % (x1,y1,x2,y2))
		self.cmd("v.net.path input=allgpx output=segment --o ","0 %s %s %s %s \n" % (x1,y1,x2,y2))
		#print "echo shortest between"+x1+","+y1+":"+x2+","+y2
		
		self.cmd("v.patch input=path,segment output=temp_path --o")
		self.cmd("g.copy vect=temp_path,path --o")
		#add to the final path the segment
		#
		x1=x2
		y1=y2
	
	self.status.set(msg="Path created",percentDone=40)
	self.cmd("v.out.ascii input=path output=/tmp/grass/app.point format=standard")
	i = 0
	coords=''
	f=open('/tmp/grass/app.point', 'r')
	h=f.readlines()
	l=len(h)
	for i in range(13,l-2):
		#jump to the 13th row
		coords=coords+re.sub(' +',',',h[i].strip())+','
	coords=coords.rstrip(',')
	f.close();
	self.cmd("r.profile input=elevation profile=%s output=/tmp/grass/path.profile" % coords)
	self.status.set(msg="Altimetric Profile created",percentDone=60)
	g = Gnuplot.Gnuplot()
	g.title('Profilo altimetrico del sentiero')
	g.xlabel('Distanza percorsa')
	g.ylabel('Altitudine')
	g('set terminal unknown')
	g('set data style lines')
	g.plot(Gnuplot.File('/tmp/grass/path.profile', title=None))
	g.hardcopy('/var/www/profile.png',terminal = 'png')
        
	self.status.set(msg="Gnuplot image generated",percentDone=70)
        self.cmd("v.out.ogr format=GML input=path dsn=out.xml  olayer=path.xml")
	self.status.set(msg="Results saved", percentDone=80)
	
        
        if "out.xml" in os.listdir(os.curdir):
            self.los.setValue("out.xml")
            return
        else:
            return "Output file not created"


