#!/usr/bin/python

# Author: Stepan Kafka

import os,time,string,sys,Gnuplot,re
from pywps.Process.Process import WPSProcess

class Process(WPSProcess):
    def __init__(self):
        WPSProcess.__init__(self,
		identifier = "paths",
        	title="Paths stored",
		abstract="Paths on the Trentino SAT network",
		version = "0.2",
        	storeSupported = True,
		statusSupported = True,
        	grassLocation="alpi")
        self.los = self.addComplexOutput(identifier = "los",
                            title = "Resulting output map",
                            formats = [{"mimeType":"text/xml"}])
    
    def execute(self):
	self.cmd("g.mapset mapset=nico")
        #:browse confirm saveas
	self.cmd("g.region -d")
	#self.cmd("g.region vect=allgpx")
	self.status.set(msg="Region setted", percentDone=20)
        self.cmd("v.out.ogr format=GML input=allgpx dsn=out.xml  olayer=path.xml")
	self.status.set(msg="Results saved", percentDone=80)
        if "out.xml" in os.listdir(os.curdir):
            self.los.setValue("out.xml")
            return
        else:
            return "Output file not created"


